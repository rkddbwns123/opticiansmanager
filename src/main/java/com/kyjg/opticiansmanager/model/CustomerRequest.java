package com.kyjg.opticiansmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerRequest {
    @NotNull
    @Length(min = 1, max = 20)
    private String customerName;
    @NotNull
    @Length(min = 11, max = 20)
    private String customerPhone;
    @NotNull
    private float figureEyeRight;
    @NotNull
    private float figureEyeLeft;

}
