package com.kyjg.opticiansmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CustomerResponse {
    private Long id;
    private String CustomerName;
    private String CustomerPhone;
    private LocalDate dateLast;
    private String figureEye;
    private String needGlassesLeft;
    private String needGlassesRight;
    private Boolean isSuperman;
}
